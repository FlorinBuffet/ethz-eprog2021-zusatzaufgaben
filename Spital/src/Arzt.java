
public class Arzt extends Angestellte{
	
	public Arzt(int years) {
		super(years);
	}
	
	@Override
	public double getSalary() {
		return super.getSalary()+getYears()*5000;
	}
	
	public void treatPatient() {
		System.out.println("Sie sind geheilt!");
	}

}
