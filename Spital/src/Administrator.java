
public class Administrator extends Angestellte {
	
	public Administrator(int years) {
		super(years);
	}
	
	@Override
	public double getSalary() {
		return super.getSalary()+5000;
	}
	
	public void processBills() {
		System.out.println("Bezahlen Sie umgehend!");
	}
}
