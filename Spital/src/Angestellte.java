
public class Angestellte{
	private int years;
	
	public Angestellte(int years) {
		this.years = years;
	}
	
	public double getSalary() {
		return 90000.00;
	}
	
	public int getHours() {
		return 42;
	}

	public int getYears() {
		return years;
	}
	
	public String getVacationForm() {
		return "green";
	}
	
	public int getVacationDays() {
		return 20 + getSeniorityBonus();
	}
	
	public int getSeniorityBonus() {
		return 2*years;
	}
}
