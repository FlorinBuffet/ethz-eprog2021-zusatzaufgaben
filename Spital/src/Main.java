
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FaGe fage1 = new FaGe(10);
		System.out.println(fage1.getHours());
		System.out.println(fage1.getSalary());
		System.out.println(fage1.getVacationDays());
		fage1.workAtStation();
		
		Arzt arzt1 = new Arzt(5);
		System.out.println(arzt1.getSalary());
		System.out.println(arzt1.getYears());
		arzt1.treatPatient();
		
		OPFachkraft opfk1 = new OPFachkraft(10);
		System.out.println(opfk1.getSalary());
		opfk1.manageOP();
		
		Administrator admin1 = new Administrator(25);
		System.out.println(admin1.getSalary());
		System.out.println(admin1.getVacationDays());
		admin1.processBills();
		
		
	}

}
