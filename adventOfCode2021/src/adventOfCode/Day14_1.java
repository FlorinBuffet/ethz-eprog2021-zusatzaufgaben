package adventOfCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day14_1 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day14_1.txt"));
		String start = file.nextLine();
		file.nextLine();
		String[] commands = new String[100];
		int counter = 0;
		while (file.hasNextLine()) {
			commands[counter] = file.nextLine();
			counter++;
		}

		String temp = start;
		for (int i = 0; i < 40; i++) {
			System.out.println("Step "+i);
			temp = nextStep(temp, commands);
		}
		System.out.println(temp);
		int mostcommon = Integer.MIN_VALUE;
		int leastcommon = Integer.MAX_VALUE;
		for (int i = 0; i<temp.length(); i++) {
			char toFind = temp.charAt(i);
			int count = 0;
			for (int j = 0; j<temp.length(); j++) {
				if (toFind == temp.charAt(j)) count++;
			}
			mostcommon = Math.max(mostcommon, count);
			leastcommon = Math.min(leastcommon, count);
		}
		System.out.println(mostcommon-leastcommon);
	}

	public static String nextStep(String start, String[] commands) {
		for (int i = 0; i < start.length()-1; i++) {
			System.out.println(i+"/"+(start.length()-1));
			String toFind = "" + start.charAt(i) + start.charAt(i + 1);
			char toInsert = findLetter(toFind, commands);
			if (toInsert!='-') {
				start = start.substring(0, i+1) + toInsert + start.substring(i+1);
				i++;
			}
		}
		return start;
	}

	public static char findLetter(String toFind, String[] commands) {
		for (String command : commands) {
			String toMatch = command.substring(0, 2);
			if (toMatch.equals(toFind)) {
				return command.charAt(6);
			}
		}
		return '-';
	}

}
