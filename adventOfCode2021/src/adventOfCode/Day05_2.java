package adventOfCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day05_2 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day05_1.txt"));
		int max = 0;
		while (file.hasNextLine()) {
			String lineString = file.nextLine();
			Scanner line = new Scanner(lineString);
			String firstPart = line.next();
			int highestNumberInFirstPart = Math.max(Integer.parseInt(firstPart.substring(0, firstPart.indexOf(','))),
					Integer.parseInt(firstPart.substring(firstPart.indexOf(',') + 1)));
			line.next();
			String secondPart = line.next();
			int highestNumberInSecondPart = Math.max(Integer.parseInt(secondPart.substring(0, secondPart.indexOf(','))),
					Integer.parseInt(secondPart.substring(secondPart.indexOf(',') + 1)));
			max = Math.max(max, Math.max(highestNumberInFirstPart, highestNumberInSecondPart));
		}
		file.close();
		int[][] field = new int[max + 1][max + 1];
		for (int[] line : field) {
			for (int spot : line) {
				spot = 0;
			}
		}
		Scanner file2 = new Scanner(new File("data/Day05_1.txt"));
		while (file2.hasNextLine()) {
			String lineString = file2.nextLine();
			Scanner line = new Scanner(lineString);
			String firstPart = line.next();
			int firstPartLine = Integer.parseInt(firstPart.substring(0, firstPart.indexOf(',')));
			int firstPartColumn = Integer.parseInt(firstPart.substring(firstPart.indexOf(',') + 1));
			line.next();
			String secondPart = line.next();
			int secondPartLine = Integer.parseInt(secondPart.substring(0, secondPart.indexOf(',')));
			int secondPartColumn = Integer.parseInt(secondPart.substring(secondPart.indexOf(',') + 1));
			if ((firstPartLine != secondPartLine) && (firstPartColumn != secondPartColumn)) {
				int stepLine = -1;
				int stepColumn = -1;
				int partLine = firstPartLine;
				int partColumn = firstPartColumn;
				if (secondPartLine > firstPartLine)
					stepLine = 1;
				if (secondPartColumn > firstPartColumn)
					stepColumn = 1;
				boolean notFinished = true;
				while (notFinished) {
					if (partLine == secondPartLine)
						notFinished = false;
					field[partLine][partColumn]++;
					partLine += stepLine;
					partColumn += stepColumn;

				}
			} else if (firstPartLine == secondPartLine) {
				int partLine = firstPartLine;
				int minPartColumn = Math.min(firstPartColumn, secondPartColumn);
				int maxPartColumn = Math.max(firstPartColumn, secondPartColumn);
				for (int i = minPartColumn; i <= maxPartColumn; i++) {
					field[partLine][i]++;
				}
			} else if (firstPartColumn == secondPartColumn) {
				int partColumn = firstPartColumn;
				int minPartLine = Math.min(firstPartLine, secondPartLine);
				int maxPartLine = Math.max(firstPartLine, secondPartLine);
				for (int i = minPartLine; i <= maxPartLine; i++) {
					field[i][partColumn]++;
				}
			}
		}
		int counter = 0;
		for (int[] line : field) {
			for (int spot : line) {
				if (spot >= 2)
					counter++;
			}
		}
		System.out.println(counter);
	}

}
