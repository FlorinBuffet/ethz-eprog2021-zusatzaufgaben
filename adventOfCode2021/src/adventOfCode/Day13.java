package adventOfCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Day13 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day13_1.txt"));
		boolean[][] field = new boolean[1500][1500];
		String[] commands = new String[100];
		int posInCommand = 0;
		while (file.hasNextLine()) {
			String lineString = file.nextLine();
			if (lineString.startsWith("fold")) {
				commands[posInCommand] = lineString;
				posInCommand++;
			} else if (lineString != "") {
				field = buildField(field, lineString);
			}
		}
		int i = 0;
		while (commands[i] != null) {
			String input = commands[i];
			String direction = input.substring(input.indexOf('=') - 1, input.indexOf('='));
			int position = Integer.parseInt(input.substring(input.indexOf('=') + 1));
			if (direction.equals("x")) {
				field = foldX(field, position);
			} else if (direction.equals("y")) {
				field = foldY(field, position);
			}
			System.out.println(input + ": " +countTrue(field));
			i++;
		}
		System.out.println();
		System.out.println("------------------------------------------------------------------------------");
		print(field);
		System.out.println("------------------------------------------------------------------------------");
	}

	public static void print(boolean[][] field) {
		for (int j = 0; j < 100; j++) {
			String outputString = "";
			int occurrence = 0;
			for (int i = 0; i < 100; i++) {
				if (field[i][j]) {
					outputString = outputString + "[]";
					occurrence++;
				} else {
					outputString = outputString + "  ";
				}
			}
			outputString = outputString.strip();
			if (occurrence > 0)
				System.out.println(outputString);
		}
	}

	public static int countTrue(boolean[][] field) {
		int counter = 0;
		for (int i = 0; i < field.length; i++) {
			for (int j = 0; j < field[i].length; j++) {
				if (field[i][j])
					counter++;
			}
		}
		return counter;
	}

	public static boolean[][] foldX(boolean[][] field, int position) {
		for (int j = 0; j < field[1].length; j++) {
			for (int i = position + 1; i < field.length; i++) {
				if (field[i][j]) {
					field[position - (i - position)][j] = true;
					field[i][j] = false;
				}
			}
		}
		return field;
	}

	public static boolean[][] foldY(boolean[][] field, int position) {
		for (int i = 0; i < field.length; i++) {
			for (int j = position + 1; j < field[i].length; j++) {
				if (field[i][j]) {
					field[i][position - (j - position)] = true;
					field[i][j] = false;
				}
			}
		}
		return field;
	}

	public static boolean[][] buildField(boolean[][] field, String linestring) {
		int[] inputs = Arrays.stream(linestring.split(",")).mapToInt(Integer::parseInt).toArray();
		field[inputs[0]][inputs[1]] = true;
		return field;
	}

}
