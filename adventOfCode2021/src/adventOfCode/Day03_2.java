package adventOfCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Day03_2 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day03_1.txt"));
		int totalLines = 0;
		while (file.hasNextLine()) {
			file.nextLine();
			totalLines++;
		}
		file.close();

		Scanner file2 = new Scanner(new File("data/Day03_1.txt"));
		String[] numbers = new String[totalLines];
		int counter = 0;
		while (file2.hasNextLine()) {
			numbers[counter] = file2.nextLine();
			counter++;
		}
		file2.close();

		String[] workArray = Arrays.copyOf(numbers, totalLines);
		String resultOxygen = "";
		for (int i = 0; i < 12; i++) {
			int mostUsedIs1 = 0;
			for (String work : workArray) {
				if (work == "") {

				} else if (work.charAt(i) == '1') {
					mostUsedIs1++;
				} else if (work.charAt(i) == '0') {
					mostUsedIs1--;
				}
			}
			char toEliminate = '0';
			if (mostUsedIs1<0) toEliminate = '1';
			int stillAvailable = 0;
			for (int j = 0; j<totalLines; j++) {
				if (workArray[j] == "") {
					
				}else if (workArray[j].charAt(i)==toEliminate) {
					workArray[j] = "";
				}else {
					stillAvailable++;
					resultOxygen = workArray[j];
				}
			}
			if (stillAvailable == 1) {
				break;
			}
		}
		
		workArray = Arrays.copyOf(numbers, totalLines);
		String resultCO2 = "";
		for (int i = 0; i < 12; i++) {
			int mostUsedIs1 = 0;
			for (String work : workArray) {
				if (work == "") {

				} else if (work.charAt(i) == '1') {
					mostUsedIs1++;
				} else if (work.charAt(i) == '0') {
					mostUsedIs1--;
				}
			}
			char toEliminate = '1';
			if (mostUsedIs1<0) toEliminate = '0';
			int stillAvailable = 0;
			for (int j = 0; j<totalLines; j++) {
				if (workArray[j] == "") {
					
				}else if (workArray[j].charAt(i)==toEliminate) {
					workArray[j] = "";
				}else {
					stillAvailable++;
					resultCO2 = workArray[j];
				}
			}
			if (stillAvailable == 1) {
				break;
			}
		}
		System.out.println(Integer.parseInt(resultOxygen, 2) * Integer.parseInt(resultCO2, 2));
	}

}
