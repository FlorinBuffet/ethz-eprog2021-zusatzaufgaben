package adventOfCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day07_2 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day07_1.txt"));
		String input = file.next();
		file.close();
		String temp = input;
		int crabCount = (int) (1 + temp.chars().filter(ch -> ch == ',').count());
		int[] crabs = new int[crabCount];
		int minPos = Integer.MAX_VALUE;
		int maxPos = Integer.MIN_VALUE;
		for (int i = 0; i < crabCount - 1; i++) {
			crabs[i] = Integer.parseInt(temp.substring(0, temp.indexOf(',')));
			temp = temp.substring(temp.indexOf(',') + 1);
			minPos = Math.min(minPos, crabs[i]);
			maxPos = Math.max(maxPos, crabs[i]);
		}
		crabs[crabCount - 1] = Integer.parseInt(temp);
		minPos = Math.min(minPos, crabs[crabCount-1]);
		maxPos = Math.max(maxPos, crabs[crabCount-1]);

		long minFuel = Long.MAX_VALUE;
		for (int i = minPos; i <= maxPos; i++) {
			long fuel = 0;
			for (int crab : crabs) {
				fuel += calcFuelForDistance(Math.abs(crab - i));
			}
			minFuel = Math.min(minFuel, fuel);
		}
		System.out.println("Min Fuel: " + minFuel);

	}

	public static int calcFuelForDistance(int distance) {
		int priceForThisMovement = 1;
		int sum = 0;
		for (int i = 1; i <= distance; i++) {
			sum += priceForThisMovement;
			priceForThisMovement++;
		}
		return sum;
	}

}
