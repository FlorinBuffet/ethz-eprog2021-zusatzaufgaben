package adventOfCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day03_1 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day03_1.txt"));

		int[] usage = new int[12];

		while (file.hasNextLine()) {
			String value = file.next();
			for (int i = 0; i < 12; i++) {
				if (value.charAt(i) == '1') {
					usage[i]++;
				} else if (value.charAt(i) == '0') {
					usage[i]--;
				}
			}
		}
		String gamma = "";
		String epsilon = "";
		for (int i : usage) {
			if (i > 0) {
				gamma = gamma.concat("1");
				epsilon = epsilon.concat("0");
			} else if (i < 0) {
				gamma = gamma.concat("0");
				epsilon = epsilon.concat("1");
			}
		}
		System.out.println(Integer.parseInt(gamma, 2) * Integer.parseInt(epsilon, 2));
	}

}
