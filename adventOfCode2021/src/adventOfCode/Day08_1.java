package adventOfCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Day08_1 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day08_1.txt"));
		int count = 0;
		while (file.hasNextLine()) {
			String lineString = file.nextLine();
			String part2 = lineString.substring(lineString.indexOf('|') + 2, lineString.length());
			String[] part2Single = part2.split(" ");
			for (String part : part2Single) {
				int length = part.length();
				if (length == 2 || length == 3 || length == 4 || length == 7)
					count++;
			}
		}
		file.close();
		System.out.println(count);
	}

}
