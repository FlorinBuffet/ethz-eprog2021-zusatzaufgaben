package adventOfCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day02_1 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day02_1.txt"));

		int distance = 0;
		int depth = 0;

		while (file.hasNextLine()) {
			String lineString = file.nextLine();
			Scanner line = new Scanner(lineString);
			String direction = line.next();
			switch (direction) {
			case "forward":
				distance += line.nextInt();
				break;
			case "down":
				depth += line.nextInt();
				break;
			case "up":
				depth -= line.nextInt();
				break;
			}
			line.close();
		}
		System.out.println(distance * depth);
		file.close();
	}

}
