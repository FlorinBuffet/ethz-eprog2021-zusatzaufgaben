package adventOfCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Day09_2 {

	public static final int inputlines = 100;
	public static final int inputrows = 100;

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day09_1.txt"));

		int[][] cavemap = new int[inputlines][];
		int counter = 0;
		while (file.hasNextLine()) {
			char[] temp = file.nextLine().toCharArray();
			cavemap[counter] = new int[inputrows];
			for (int i = 0; i < inputrows; i++) {
				cavemap[counter][i] = temp[i] - '0';
			}
			counter++;
		}

		ArrayList<Integer> results = new ArrayList<Integer>();
		for (int i = 0; i < inputlines; i++) {
			for (int j = 0; j < inputrows; j++) {
				if (heightIfLowest(i, j, cavemap) >= 0) {
					results.add(basinSize(i, j, cavemap));
				}
			}
		}
		System.out.println(results.stream().sorted(Collections.reverseOrder()).limit(3).reduce((a, b) -> a * b));
	}

	public static int basinSize(int line, int row, int[][] map) {
		boolean[][] checked = new boolean[inputlines][inputrows];
		checked = basinSizeRecursive(line, row, map, checked);
		int counter = 0;
		for (boolean[] thisLine : checked) {
			for (boolean item : thisLine) {
				if (item)
					counter++;
			}
		}
		return counter;
	}

	public static boolean[][] basinSizeRecursive(int line, int row, int[][] map, boolean[][] checked) {
		checked[line][row] = true;
		try {
			if (map[line + 1][row] > map[line][row] && map[line + 1][row] != 9) {
				checked = basinSizeRecursive(line + 1, row, map, checked);
			}
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		try {
			if (map[line - 1][row] > map[line][row] && map[line - 1][row] != 9) {
				checked = basinSizeRecursive(line - 1, row, map, checked);
			}
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		try {
			if (map[line][row + 1] > map[line][row] && map[line][row + 1] != 9) {
				checked = basinSizeRecursive(line, row + 1, map, checked);
			}
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		try {
			if (map[line][row - 1] > map[line][row] && map[line][row - 1] != 9) {
				checked = basinSizeRecursive(line, row - 1, map, checked);
			}
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		return checked;
	}

	public static int heightIfLowest(int line, int row, int[][] map) {
		try {
			if (map[line][row] >= map[line + 1][row])
				return -1;
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		try {
			if (map[line][row] >= map[line - 1][row])
				return -1;
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		try {
			if (map[line][row] >= map[line][row + 1])
				return -1;
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		try {
			if (map[line][row] >= map[line][row - 1])
				return -1;
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		return map[line][row];
	}

}
