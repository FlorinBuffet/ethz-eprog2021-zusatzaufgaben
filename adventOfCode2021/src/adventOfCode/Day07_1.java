package adventOfCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day07_1 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day07_1.txt"));
		String input = file.next();
		file.close();
		String temp = input;
		int crabCount = (int) (1 + temp.chars().filter(ch -> ch == ',').count());
		int[] crabs = new int[crabCount];
		for (int i = 0; i < crabCount - 1; i++) {
			crabs[i] = Integer.parseInt(temp.substring(0, temp.indexOf(',')));
			temp = temp.substring(temp.indexOf(',') + 1);
		}
		crabs[crabCount - 1] = Integer.parseInt(temp);

		long minFuel = Long.MAX_VALUE;
		for (int i = 0; i < 1000; i++) {
			long fuel = 0;
			for (int crab : crabs) {
				fuel += Math.abs(crab - i);
			}
			minFuel = Math.min(minFuel, fuel);
		}
		System.out.println("Min Fuel: " + minFuel);

	}

}
