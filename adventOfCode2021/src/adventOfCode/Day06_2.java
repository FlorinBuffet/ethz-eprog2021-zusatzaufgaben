package adventOfCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Day06_2 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day06_1.txt"));
		String allFishBackup = file.next();
		String allFish = allFishBackup;
		file.close();
		ArrayList<Integer> fish = new ArrayList<>();
		while (allFish.length() > 0) {
			fish.add(Integer.parseInt(allFish.substring(0, 1)));
			if (allFish.indexOf(',') == -1)
				allFish = "";
			allFish = allFish.substring(allFish.indexOf(',') + 1);

		}

		for (int i = 0; i < 256; i++) {
			System.out.println(i);
			fish = nextDay(fish, i);
		}
		System.out.println(fish.size());
	}

	public static ArrayList<Integer> nextDay(ArrayList<Integer> fish, int day) {
		int runTo = fish.size();
		for (int i = 0; i < runTo; i++) {
			fish.set(i, fish.get(i) - 1);
			if (fish.get(i) == -1) {
				fish.add(8);
				fish.set(i, 6);
			}
		}

		return fish;

	}

}
