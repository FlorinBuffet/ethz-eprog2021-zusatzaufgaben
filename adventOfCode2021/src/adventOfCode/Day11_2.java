package adventOfCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Day11_2 {
	public static final int size = 10;

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day11_1.txt"));

		int[][] octopus = new int[size][size];
		for (int i = 0; i < size; i++) {
			String lineString = file.nextLine();
			for (int j = 0; j < size; j++) {
				octopus[i][j] = (int) lineString.charAt(j) - '0';
			}
		}

		int flashes = 0;
		for (int step = 1; step <= 500; step++) {
			boolean[][] flashed = new boolean[size][size];
			octopus = increaseByOne(octopus);
			int flashesThisRound = 1;
			while (flashesThisRound > 0) {
				flashesThisRound = 0;
				for (int i = 0; i < size; i++) {
					for (int j = 0; j < size; j++) {
						if (octopus[i][j] > 9 && !flashed[i][j]) {
							flashesThisRound++;
							flashes++;
							flashed[i][j] = true;
							octopus = increaseAdjacent(octopus, i, j);
						}
					}
				}
			}
			octopus = setToZero(octopus);
			if (checkForOnlyZero(octopus))
				System.out.println(step);
		}
		System.out.println(flashes);
	}

	public static boolean checkForOnlyZero(int[][] octopus) {
		for (int[] line : octopus) {
			for (int oct : line) {
				if (oct != 0)
					return false;
			}
		}
		return true;
	}

	public static int[][] setToZero(int[][] octopus) {
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (octopus[i][j] > 9)
					octopus[i][j] = 0;
			}
		}
		return octopus;
	}

	public static void print(int[][] octopus) {
		for (int i = 0; i < size; i++) {
			System.out.println(Arrays.toString(octopus[i]));
		}
		System.out.println();
	}

	public static int[][] increaseAdjacent(int[][] octopus, int i, int j) {
		try {
			octopus[i - 1][j - 1]++;
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		try {
			octopus[i - 1][j]++;
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		try {
			octopus[i - 1][j + 1]++;
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		try {
			octopus[i][j - 1]++;
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		try {
			octopus[i][j + 1]++;
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		try {
			octopus[i + 1][j - 1]++;
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		try {
			octopus[i + 1][j]++;
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		try {
			octopus[i + 1][j + 1]++;
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		return octopus;
	}

	public static int[][] increaseByOne(int[][] octopus) {
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				octopus[i][j]++;
			}
		}
		return octopus;
	}
}
