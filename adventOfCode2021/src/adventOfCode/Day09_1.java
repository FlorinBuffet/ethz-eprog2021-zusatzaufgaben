package adventOfCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Day09_1 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day09_1.txt"));
		int[][] cavemap = new int[100][];
		int counter = 0;
		while (file.hasNextLine()) {
			char[] temp = file.nextLine().toCharArray();
			cavemap[counter] = new int[100];
			for (int i = 0; i < 100; i++) {
				cavemap[counter][i] = temp[i] - '0';
			}
			counter++;
		}

		int output = 0;
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < 100; j++) {
				output += heightIfLowest(i, j, cavemap) + 1;
			}
		}
		System.out.println(output);
	}

	public static int heightIfLowest(int line, int row, int[][] map) {
		try {
			if (map[line][row] >= map[line + 1][row])
				return -1;
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		try {
			if (map[line][row] >= map[line - 1][row])
				return -1;
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		try {
			if (map[line][row] >= map[line][row + 1])
				return -1;
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		try {
			if (map[line][row] >= map[line][row - 1])
				return -1;
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		return map[line][row];
	}

}
