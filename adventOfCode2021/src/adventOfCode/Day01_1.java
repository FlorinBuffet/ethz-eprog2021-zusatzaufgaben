package adventOfCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day01_1 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner fileScanner = new Scanner(new File("data/Day01_1.txt"));
		int increments = 0;
		int lastDepth = fileScanner.nextInt();
		while (fileScanner.hasNextInt()) {
			int tmp = fileScanner.nextInt();
			if (tmp > lastDepth)
				increments++;
			lastDepth = tmp;
		}
		System.out.println(increments);
		fileScanner.close();
	}

}
