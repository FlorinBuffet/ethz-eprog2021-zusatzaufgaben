package adventOfCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Day08_2 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day08_1.txt"));
		int result = 0;
		while (file.hasNextLine()) {
			String lineString = file.nextLine();
			String part1 = lineString.substring(0, lineString.indexOf('|') - 1);
			String[] part1Single = part1.split(" ");
			String part2 = lineString.substring(lineString.indexOf('|') + 2, lineString.length());
			String[] part2Single = part2.split(" ");
			String[] decoded = new String[10];
			while (decoded[6] == null || decoded[2] == null) {
				for (String part : part1Single) {
					char[] chars = part.toCharArray();
					Arrays.sort(chars);
					String partSorted = new String(chars);
					if (part.length() == 2) {
						decoded[1] = partSorted;
					} else if (part.length() == 3) {
						decoded[7] = partSorted;
					} else if (part.length() == 4) {
						decoded[4] = partSorted;
					} else if (part.length() == 7) {
						decoded[8] = partSorted;
					} else if (part.length() == 5 && containsItAll(part, decoded[7])) {
						decoded[3] = partSorted;
					} else if (part.length() == 5 && decoded[3] != null && charsMatching(part, decoded[4], 3)) {
						decoded[5] = partSorted;
					} else if (part.length() == 5 && decoded[5] != null) {
						decoded[2] = partSorted;
					} else if (part.length() == 6 && containsItAll(part, decoded[4])) {
						decoded[9] = partSorted;
					} else if (part.length() == 6 && containsItAll(part, decoded[1]) && decoded[9] != null) {
						decoded[0] = partSorted;
					} else if (part.length() == 6 && decoded[0] != null) {
						decoded[6] = partSorted;
					}
				}
			}
			int multiplier = 1000;
			for (int i = 0; i < part2Single.length; i++) {
				char[] chars = part2Single[i].toCharArray();
				Arrays.sort(chars);
				String partSorted = new String(chars);
				result += multiplier * (whichNumber(partSorted, decoded));
				multiplier = multiplier / 10;
			}
		}
		System.out.println(result);
	}

	public static int whichNumber(String input, String[] decoded) {
		for (int i = 0; i < decoded.length; i++) {
			if (decoded[i].equals(input))
				return i;
		}
		return -1;
	}

	public static boolean containsItAll(String toTest, String testfor) {
		if (testfor == null)
			return false;
		for (int i = 0; i < testfor.length(); i++) {
			Boolean found = false;
			for (int j = 0; j < toTest.length(); j++) {
				if (testfor.charAt(i) == toTest.charAt(j))
					found = true;
			}
			if (!found)
				return false;
		}
		return true;
	}

	public static boolean charsMatching(String toTest, String testfor, int safetyAsked) {
		int safety = 0;
		if (testfor == null)
			return false;
		for (int i = 0; i < testfor.length(); i++) {
			for (int j = 0; j < toTest.length(); j++) {
				if (testfor.charAt(i) == toTest.charAt(j))
					safety++;
			}
		}
		if (safety == safetyAsked)
			return true;
		return false;
	}

}
