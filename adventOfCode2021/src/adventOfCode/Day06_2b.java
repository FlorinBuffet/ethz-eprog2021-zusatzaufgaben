package adventOfCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day06_2b {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day06_1.txt"));
		String allFishBackup = file.next();
		String allFish = allFishBackup;
		file.close();
		long[] fish = new long[9];
		while (allFish.length() > 0) {
			switch (Integer.parseInt(allFish.substring(0, 1))) {
			case (0):
				fish[0]++;
				break;
			case (1):
				fish[1]++;
				break;
			case (2):
				fish[2]++;
				break;
			case (3):
				fish[3]++;
				break;
			case (4):
				fish[4]++;
				break;
			case (5):
				fish[5]++;
				break;
			case (6):
				fish[6]++;
				break;
			case (7):
				fish[7]++;
				break;
			case (8):
				fish[8]++;
				break;
			}

			if (allFish.indexOf(',') == -1)
				allFish = "";
			allFish = allFish.substring(allFish.indexOf(',') + 1);
		}
		for (int i = 0; i < 256; i++) {
			fish = nextDay(fish);
		}
		System.out.println(fish[0] + fish[1] + fish[2] + fish[3] + fish[4] + fish[5] + fish[6] + fish[7] + fish[8]);
	}

	public static long[] nextDay(long[] fish) {
		long noFish = fish[0];
		fish[0] = fish[1];
		fish[1] = fish[2];
		fish[2] = fish[3];
		fish[3] = fish[4];
		fish[4] = fish[5];
		fish[5] = fish[6];
		fish[6] = fish[7] + noFish;
		fish[7] = fish[8];
		fish[8] = noFish;
		return fish;
	}

}
