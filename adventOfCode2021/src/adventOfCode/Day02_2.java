package adventOfCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day02_2 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day02_1.txt"));

		int distance = 0;
		int depth = 0;
		int aim = 0;

		while (file.hasNextLine()) {
			String lineString = file.nextLine();
			Scanner line = new Scanner(lineString);
			String direction = line.next();
			switch (direction) {
			case "forward":
				int tmp = line.nextInt();
				distance += tmp;
				depth += aim * tmp;
				break;
			case "down":
				aim += line.nextInt();
				break;
			case "up":
				aim -= line.nextInt();
				break;
			}
			line.close();
		}
		System.out.println(distance * depth);
		file.close();
	}

}