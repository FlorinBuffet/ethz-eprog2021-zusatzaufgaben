package adventOfCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day04_1 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day04_1.txt"));
		int lineCount = 0;
		while (file.hasNextLine()) {
			lineCount++;
			file.nextLine();
		}
		file.close();

		int[][][] boards = new int[(lineCount - 1) / 6][5][5];
		boolean[][][] boardsChecked = new boolean[(lineCount - 1) / 6][5][5];

		Scanner file2 = new Scanner(new File("data/Day04_1.txt"));
		file2.nextLine();
		file2.nextLine();
		int counter = 0;
		while (file2.hasNextLine()) {
			String lineString = file2.nextLine();
			if (lineString != "") {
				Scanner line = new Scanner(lineString);
				for (int i = 0; i < 5; i++) {
					boards[counter / 6][counter % 6][i] = line.nextInt();
					boardsChecked[counter / 6][counter % 6][i] = false;
				}
				line.close();
			}
			counter++;
		}
		file2.close();

		Scanner file3 = new Scanner(new File("data/Day04_1.txt"));
		String wholeLine = file3.next();
		boolean noResult = true;
		int result = 0;
		int toUseMemory = 0;
		while (noResult) {
			int toUse = Integer.parseInt(wholeLine.substring(0, wholeLine.indexOf(',')));
			wholeLine = wholeLine.substring(wholeLine.indexOf(',') + 1, wholeLine.length());
			System.out.println(toUse);
			toUseMemory = toUse;
			for (int i = 0; i < boards.length; i++) {
				for (int j = 0; j < boards[i].length; j++) {
					for (int k = 0; k < boards[i][j].length; k++) {
						if (boards[i][j][k] == toUse) {
							boardsChecked[i][j][k] = true;
						}
					}
				}
			}
			for (int i = 0; i < boardsChecked.length; i++) {
				for (int j = 0; j < boardsChecked[i].length; j++) {
					int lineCounter = 0;
					for (int k = 0; k < boardsChecked[i][j].length; k++) {
						if (boardsChecked[i][j][k])
							lineCounter++;
					}
					if (lineCounter == 5) {
						result = i;
						noResult = false;
					}
				}
				for (int j = 0; j < boardsChecked[i].length; j++) {
					int lineCounter = 0;
					for (int k = 0; k < boardsChecked[i][j].length; k++) {
						if (boardsChecked[i][k][j])
							lineCounter++;
					}
					if (lineCounter == 5) {
						result = i;
						noResult = false;
					}
				}
			}
		}
		int sum = 0;
		for (int i = 0; i < boards[result].length; i++) {
			for (int j = 0; j<boards[result][i].length; j++) {
				if (!boardsChecked[result][i][j]) {
					sum = sum + boards[result][i][j];
				}
			}
		}
		System.out.println(sum*toUseMemory);
	}

}
