package adventOfCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day01_2 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner fileScanner = new Scanner(new File("data/Day01_1.txt"));
		int increments = 0;
		int firstInWindow = fileScanner.nextInt();
		int secondInWindow = fileScanner.nextInt();
		int thirdInWindow = fileScanner.nextInt();

		while (fileScanner.hasNextInt()) {
			int tmp = fileScanner.nextInt();
			if (firstInWindow + secondInWindow + thirdInWindow < secondInWindow + thirdInWindow + tmp)
				increments++;
			firstInWindow = secondInWindow;
			secondInWindow = thirdInWindow;
			thirdInWindow = tmp;
		}
		System.out.println(increments);
		fileScanner.close();

	}

}
