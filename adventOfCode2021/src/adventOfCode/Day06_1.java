package adventOfCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day06_1 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day06_1.txt"));
		String allFishBackup = file.next();
		String allFish = allFishBackup;
		file.close();
		int startFishCount = 0;
		while (allFish.length() > 0) {
			if (allFish.indexOf(',') == -1)
				allFish = "";
			allFish = allFish.substring(allFish.indexOf(',') + 1);
			startFishCount++;
		}
		int[] fish = new int[startFishCount];
		allFish = allFishBackup;
		for (int i = 0; i < startFishCount; i++) {
			fish[i] = Integer.parseInt(allFish.substring(0, 1));
			allFish = allFish.substring(allFish.indexOf(',') + 1);
		}

		for (int i = 0; i < 80; i++) {
			fish = nextDay(fish);
		}
		System.out.println(fish.length);
	}

	public static int[] nextDay(int[] fish) {
		int newFish = 0;

		for (int i = 0; i < fish.length; i++) {
			fish[i] = fish[i] - 1;
			if (fish[i] == -1) {
				newFish++;
				fish[i] = 6;
			}
		}

		int[] newAllFish = new int[fish.length + newFish];
		for (int i = 0; i < newAllFish.length; i++) {
			if (i < fish.length) {
				newAllFish[i] = fish[i];
			} else {
				newAllFish[i] = 8;
			}
		}

		return newAllFish;

	}

}
