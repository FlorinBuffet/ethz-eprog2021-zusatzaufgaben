package adventOfCode2020;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day02_2 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day02.txt"));
		int correctPW = 0;
		while (file.hasNextLine()) {
			Scanner line = new Scanner(file.nextLine());
			String counts = line.next();
			int leastAmount = Integer.parseInt(counts.substring(0, counts.indexOf('-')));
			int maxAmount = Integer.parseInt(counts.substring(counts.indexOf('-')+1));
			char toFind = line.next().charAt(0);
			String toAnalyze = line.next();
			boolean work1 = false;
			boolean work2 = false;
			if (toAnalyze.charAt(leastAmount-1)==toFind) work1 = true;
			if (toAnalyze.charAt(maxAmount-1)==toFind) work2 = true;
			if (work1^work2) correctPW++;
		}
		System.out.println(correctPW);

	}

}