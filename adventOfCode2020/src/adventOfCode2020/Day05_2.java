package adventOfCode2020;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day05_2 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day05.txt"));
		int lineCount = 0;
		while (file.hasNextLine()) {
			lineCount++;
			file.nextLine();
		}
		file.close();

		int[] seatID = new int[lineCount];
		Scanner line = new Scanner(new File("data/Day05.txt"));
		int maxSeatID = 0;
		int counter = 0;
		while (line.hasNextLine()) {
			String encoded = line.nextLine();
			seatID[counter]  = seatID(returnRow(encoded), returnColumn(encoded));
			if (seatID[counter] > maxSeatID) {
				maxSeatID = seatID[counter];
			}
			counter++;
		}
	
		boolean lastlastUsed = false;
		boolean lastUsed = false;
		boolean used = false;
		for (int i = 0; i<maxSeatID; i++) {
			lastlastUsed = lastUsed;
			lastUsed = used;
			used = isUsed(i,seatID);
			//System.out.println(used);
			if (lastlastUsed && used && !lastUsed) {
				System.out.println(i-1);
			}
		}
		
	}
	
	public static boolean isUsed(int seatIDsearched, int[] seatID) {
		for (int i = 0; i<seatID.length; i++) {
			if (seatID[i]==seatIDsearched) return true;
		}
		return false;
	}

	public static int returnRow(String encoded) {
		int minRow = 0;
		int maxRow = 127;

		while (encoded.length() > 0) {
			char decision = encoded.substring(0, 1).charAt(0);
			encoded = encoded.substring(1, encoded.length());
			if (decision == 'F') {
				maxRow = (maxRow - minRow) / 2 + minRow;
			} else if (decision == 'B') {
				minRow = ((maxRow + 1) - minRow) / 2 + minRow;
			}
		}
		return minRow;
	}

	public static int returnColumn(String encoded) {
		int minRow = 0;
		int maxRow = 7;

		while (encoded.length() > 0) {
			char decision = encoded.substring(0, 1).charAt(0);
			encoded = encoded.substring(1, encoded.length());
			if (decision == 'L') {
				maxRow = (maxRow - minRow) / 2 + minRow;
			} else if (decision == 'R') {
				minRow = ((maxRow + 1) - minRow) / 2 + minRow;
			}
		}
		return minRow;
	}

	public static int seatID(int row, int column) {
		return row * 8 + column;
	}

}
