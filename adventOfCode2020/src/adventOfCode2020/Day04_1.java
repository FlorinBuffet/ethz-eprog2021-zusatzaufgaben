package adventOfCode2020;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day04_1 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day04.txt"));
		//Scanner file = new Scanner(new File("data/Test.txt"));
		
		boolean byr = false;
		boolean iyr = false;
		boolean eyr = false;
		boolean hgt = false;
		boolean hcl = false;
		boolean ecl = false;
		boolean pid = false;
		boolean cid = false;
		
		int validPassports = 0;
		
		while (file.hasNextLine()) {
			String lineString = file.nextLine();
			if (lineString == "") {
				if (byr&&iyr&&eyr&&hgt&&hcl&&ecl&&pid) validPassports++;
				byr = false;
				iyr = false;
				eyr = false;
				hgt = false;
				hcl = false;
				ecl = false;
				pid = false;
				cid = false;
			}else {
				Scanner line = new Scanner(lineString);
				while (line.hasNext()) {
					String found = line.next();
					System.out.println(found);
					String value = found.substring(0, found.indexOf(':'));
					switch (value) {
					case ("byr"):
						byr = true;
						break;
					case ("iyr"):
						iyr = true;
						break;
					case ("eyr"):
						eyr = true;
						break;
					case ("hgt"):
						hgt = true;
						break;
					case ("hcl"):
						hcl = true;
						break;
					case ("ecl"):
						ecl = true;
						break;
					case ("pid"):
						pid = true;
						break;
					case ("cid"):
						cid = true;
						break;
					}
				}
			}
		}
		System.out.println(validPassports);
	}

}
