package adventOfCode2020;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day05_1 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner line = new Scanner(new File("data/Day05.txt"));
		int maxSeatID = 0;
		while (line.hasNextLine()) {
			String encoded = line.nextLine();
			int seatID = seatID(returnRow(encoded), returnColumn(encoded));
			if (seatID > maxSeatID) {
				maxSeatID = seatID;
			}
		}
		System.out.println(maxSeatID);
	}

	public static int returnRow(String encoded) {
		int minRow = 0;
		int maxRow = 127;

		while (encoded.length() > 0) {
			char decision = encoded.substring(0, 1).charAt(0);
			encoded = encoded.substring(1, encoded.length());
			if (decision == 'F') {
				maxRow = (maxRow - minRow) / 2 + minRow;
			} else if (decision == 'B') {
				minRow = ((maxRow + 1) - minRow) / 2 + minRow;
			}
		}
		return minRow;
	}

	public static int returnColumn(String encoded) {
		int minRow = 0;
		int maxRow = 7;

		while (encoded.length() > 0) {
			char decision = encoded.substring(0, 1).charAt(0);
			encoded = encoded.substring(1, encoded.length());
			if (decision == 'L') {
				maxRow = (maxRow - minRow) / 2 + minRow;
			} else if (decision == 'R') {
				minRow = ((maxRow + 1) - minRow) / 2 + minRow;
			}
		}
		return minRow;
	}

	public static int seatID(int row, int column) {
		return row * 8 + column;
	}

}
