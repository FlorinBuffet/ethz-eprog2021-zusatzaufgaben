package adventOfCode2020;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day01_2 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/Day01.txt"));

		while (file.hasNextInt()) {
			Scanner file2 = new Scanner(new File("data/Day01.txt"));
			int number1 = file.nextInt();
			while (file2.hasNextInt()) {
				Scanner file3 = new Scanner(new File("data/Day01.txt"));
				int number2 = file2.nextInt();
				while (file3.hasNextInt()) {
					int number3 = file3.nextInt();
					if (number1 + number2 + number3 == 2020) {
						System.out.println(
								number1 + " " + number2 + " " + number3 + " gibt: " + number1 * number2 * number3);
					}
				}
			}
		}
	}

}
