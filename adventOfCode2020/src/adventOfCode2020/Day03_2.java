package adventOfCode2020;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day03_2 {

	public static void main(String[] args) throws FileNotFoundException {
		
		long result = 1;
		for (int i = 0; i < 4; i++) {
			Scanner file = new Scanner(new File("data/Day03.txt"));
			//Scanner file = new Scanner(new File("data/Test.txt"));
			file.nextLine();
			int treesHit = 0;
			int movementRight = 0;
			int posFromLeft = 0;
			switch (i) {
			case 0:
				movementRight = 1;
				break;
			case 1:
				movementRight = 3;
				break;
			case 2:
				movementRight = 5;
				break;
			case 3:
				movementRight = 7;
				break;
			}
			while (file.hasNextLine()) {
				posFromLeft += movementRight;
				posFromLeft = posFromLeft % 31;
				String line = file.nextLine();
				if (line.charAt(posFromLeft) == '#') {
					treesHit++;
				}
			}
			System.out.println(treesHit);
			result = result*treesHit;
		}
		
		
		Scanner file = new Scanner(new File("data/Day03.txt"));
		//Scanner file = new Scanner(new File("data/Test.txt"));
		file.nextLine();
		int posFromLeft = 0;
		int treesHit = 0;
		while (file.hasNextLine()) {
			file.nextLine();
			posFromLeft += 1;
			posFromLeft = posFromLeft % 31;
			String line = file.nextLine();
			if (line.charAt(posFromLeft) == '#') {
				treesHit++;
			}
		}
		System.out.println(treesHit);
		result = result*treesHit;
		System.out.println(result);
	}

}
